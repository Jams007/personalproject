const {categoryEventsDataLoader} = require('../models')

module.exports = {
  Query: {
    getCategory: async (parent, {_id}, context) => {
      let response = await context.models.Categories.getCategory(_id)
      return response
    },
    listCategories: async (parent, {limit, offset}, context) => {
      let response = await context.models.Categories.listCategories({limit, offset})
      return response.categories
    }
  },
  Category: {
    events: async (parent) => {
      let response = await categoryEventsDataLoader.load([parent.id])
      return response
    }
  }
}
