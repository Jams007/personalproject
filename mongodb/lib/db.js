'use strict'
const Promise = require('bluebird')
const mongoose = require('mongoose')
const co = require('co')
const env = process.env.NODE_ENV || 'development'
const Event = require('../models/event')
const Category = require('../models/category')
const Owner = require('../models/owner')
require('dotenv').config()
let mongoConnection

class Db {
  /**
   * @description initialize all the fields and methods require for the creation/query,setup of a mongodb database.
   * @method constructor
   * @param {} none.
   * @return {object} groupByMonth, a promise with the array group by months.
   */
  constructor () {
    if (env === 'production') {
      mongoConnection = process.env.MDB_PROD
    } else {
      mongoConnection = process.env.MDB_DEV
    }
  }

  /**
   * @description connect to the production, development or testing database.
   * @method connect
   * @param {object} callback. the optional callback to return with this model
   * @return {promise} setup(), a hybrid promise or callback in the setup function or outside depending of the setup value.
   */
  connect (callback) {
    let db = this
    let getConnection = co.wrap(function * () {
      try {
        yield mongoose.connect(mongoConnection, { poolSize: 6, useNewUrlParser: true})
        if (mongoose.connection.readyState === 1) {
          db.setConnected(true)
          // mongoose.set('debug', true)
          console.log('Mongodb connected !!!')
          return Promise.resolve(mongoose.connection)
        } else {
          db.setConnected(false)
          console.log('Mongodb NOT connected !!!')
          return Promise.reject(new Error(`Mongodb NOT connected`))
        }
      } catch (err) {
        return Promise.reject(new Error(`internal error: ${err.message}`))
      }
    })
    return Promise.resolve(getConnection()).asCallback(callback)
  }

  /**
   * @description close the current connection to the database.
   * @method disconnect
   * @param {object} callback. the optional callback to return with this model
   * @return {promise} setup(), a hybrid promise or callback in the close connection promise resolution.
   */
  disconnect (callback) {
    let db = this
    let stopConnection = co.wrap(function * () {
      if (!db.connected) {
        return Promise.reject(new Error('not connected')).asCallback(callback)
      }
      yield mongoose.disconnect()
      // console.log(mongoose.connection.readyState) /*0: disconnected, 1 connected*/
      console.log('mongodb connection closed')
      db.setConnected(false)
      return Promise.resolve()
    })
    return Promise.resolve(stopConnection()).asCallback(callback)
  }

  setConnected (value) {
    this.connected = value
  }

  /**
   * @description save a new event.
   * @method saveEvent
   * @param {object} callback. the optional callback to return with this model.
   * @param {object} publicationObject. the object of the publication we are gonna save.
   * @param {string} str. the type of publication we are gonna save.
   * @return {promise} setup(), a hybrid promise or callback in the close connection promise resolution.
   */
  saveEvent (eventObject, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }
    let tasks = co.wrap(function * () {
      try {
        let event = yield Event.findOne({ name: eventObject.name, address: eventObject.address })
        if (event) {
          return Promise.reject(new Error(`publication source: ${eventObject.name} already taken for this company`))
        }
        event = new Event({
          categoryId: eventObject.categoryId,
          text: eventObject.date,
          popularity: eventObject.popularity,
          name: eventObject.name,
          description: eventObject.description,
          shortDescription: eventObject.shortDescription,
          imgURL: eventObject.imgURL,
          eventURL: eventObject.eventURL,
          address: eventObject.address,
          location: {
            type: 'Point',
            coordinates: eventObject.coordinates// [36.098948, -112.110492]
          }
        })
        event = yield event.save()
        return Promise.resolve(event)
      } catch (err) {
        console.log(err)
        return Promise.reject(new Error(`internal error: ${err.message}`))
      }
    })
    return Promise.resolve(tasks()).asCallback(callback)
  }

  /**
   * @description get a news from database with specific query.
   * @method getEvent
   * @param {object} callback. the optional callback to return with this model.
   * @param {object} query. the query that we are going to use to find the Version.
   * @return {promise} tasks(), a hybrid promise or callback that resolve the search in database.
   */
  getEvent (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }
    let tasks = co.wrap(function * () {
      try {
        let event = yield Event.findById(id)
        if (event) {
          return Promise.resolve(event)
        }
        return Promise.resolve()
      } catch (err) {
        return Promise.reject(new Error(`internal error: ${err.message}`))
      }
    })
    return Promise.resolve(tasks()).asCallback(callback)
  }

  /**
   * @description list all News from database with specific query.
   * @method listEvents
   * @param {object} callback. the optional callback to return with this model.
   * @param {object} query. the query that we are going to use to find the history.
   * @return {promise} tasks(), a hybrid promise or callback that resolve the search in database.
   */
  listEvents (query, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }
    let tasks = co.wrap(function * () {
      try {
        let where = (query && query.where) ? query.where : null
        let select = (query && query.select) ? query.select : null
        let options = (query && query.options) ? query.options : null
        let news = yield Event.find(where, select, options)
        if (news) {
          return Promise.resolve(news)
        }
        return Promise.resolve([])
      } catch (err) {
        return Promise.reject(new Error(`internal error: ${err.message}`))
      }
    })
    return Promise.resolve(tasks()).asCallback(callback)
  }

  /**
   * @description count all events with specific query.
   * @method countEvents
   * @param {object} callback. the optional callback to return with this model.
   * @param {object} query. the query that we are going to use to find the user.
   * @return {promise} tasks(), a hybrid promise or callback that resolve the search in database.
   */
  countEvents (query, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }
    let tasks = co.wrap(function * () {
      try {
        let where = (query && query.where) ? query.where : null
        let select = (query && query.select) ? query.select : null
        let options = (query && query.options) ? query.options : null
        let count = yield Event.countDocuments(where, select, options)
        if (count) {
          return Promise.resolve(count)
        }
        return Promise.resolve(0)
      } catch (err) {
        return Promise.reject(new Error(`internal error: ${err.message}`))
      }
    })
    return Promise.resolve(tasks()).asCallback(callback)
  }

  /**
   * @description save a new event.
   * @method saveCategory
   * @param {object} callback. the optional callback to return with this model.
   * @param {object} publicationObject. the object of the publication we are gonna save.
   * @param {string} str. the type of publication we are gonna save.
   * @return {promise} setup(), a hybrid promise or callback in the close connection promise resolution.
   */
  saveCategory (categoryObject, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }
    let tasks = co.wrap(function * () {
      try {
        let category = yield Category.findOne({ name: categoryObject.name })
        if (category) {
          return Promise.reject(new Error(`publication source: ${categoryObject.name} already taken for this company`))
        }
        category = new Category({
          name: categoryObject.name
        })
        category = yield category.save()
        return Promise.resolve(category)
      } catch (err) {
        console.log(err)
        return Promise.reject(new Error(`internal error: ${err.message}`))
      }
    })
    return Promise.resolve(tasks()).asCallback(callback)
  }

  /**
   * @description get a news from database with specific query.
   * @method getCategory
   * @param {object} callback. the optional callback to return with this model.
   * @param {object} query. the query that we are going to use to find the Version.
   * @return {promise} tasks(), a hybrid promise or callback that resolve the search in database.
   */
  getCategory (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }
    let tasks = co.wrap(function * () {
      try {
        let category = yield Category.findById(id)
        if (category) {
          return Promise.resolve(category)
        }
        return Promise.resolve()
      } catch (err) {
        return Promise.reject(new Error(`internal error: ${err.message}`))
      }
    })
    return Promise.resolve(tasks()).asCallback(callback)
  }

  /**
   * @description list all News from database with specific query.
   * @method listCategory
   * @param {object} callback. the optional callback to return with this model.
   * @param {object} query. the query that we are going to use to find the history.
   * @return {promise} tasks(), a hybrid promise or callback that resolve the search in database.
   */
  listCategories (query, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }
    let tasks = co.wrap(function * () {
      try {
        let where = (query && query.where) ? query.where : null
        let select = (query && query.select) ? query.select : null
        let options = (query && query.options) ? query.options : null
        let category = yield Category.find(where, select, options)
        if (category) {
          return Promise.resolve(category)
        }
        return Promise.resolve([])
      } catch (err) {
        return Promise.reject(new Error(`internal error: ${err.message}`))
      }
    })
    return Promise.resolve(tasks()).asCallback(callback)
  }

  /**
   * @description get a news from database with specific query.
   * @method getOwner
   * @param {object} callback. the optional callback to return with this model.
   * @param {object} query. the query that we are going to use to find the Version.
   * @return {promise} tasks(), a hybrid promise or callback that resolve the search in database.
   */
  getOwner (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }
    let tasks = co.wrap(function * () {
      try {
        let owner = yield Owner.findById(id)
        if (owner) {
          return Promise.resolve(owner)
        }
        return Promise.resolve()
      } catch (err) {
        return Promise.reject(new Error(`internal error: ${err.message}`))
      }
    })
    return Promise.resolve(tasks()).asCallback(callback)
  }

  /**
   * @description list all News from database with specific query.
   * @method listOwners
   * @param {object} callback. the optional callback to return with this model.
   * @param {object} query. the query that we are going to use to find the history.
   * @return {promise} tasks(), a hybrid promise or callback that resolve the search in database.
   */
  listOwners (query, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }
    let tasks = co.wrap(function * () {
      try {
        let where = (query && query.where) ? query.where : null
        let select = (query && query.select) ? query.select : null
        let options = (query && query.options) ? query.options : null
        let owners = yield Owner.find(where, select, options)
        if (owners) {
          return Promise.resolve(owners)
        }
        return Promise.resolve([])
      } catch (err) {
        return Promise.reject(new Error(`internal error: ${err.message}`))
      }
    })
    return Promise.resolve(tasks()).asCallback(callback)
  }

  /**
   * @description count all events with specific query.
   * @method countOwners
   * @param {object} callback. the optional callback to return with this model.
   * @param {object} query. the query that we are going to use to find the user.
   * @return {promise} tasks(), a hybrid promise or callback that resolve the search in database.
   */
  countOwners (query, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('not connected')).asCallback(callback)
    }
    let tasks = co.wrap(function * () {
      try {
        let where = (query && query.where) ? query.where : null
        let select = (query && query.select) ? query.select : null
        let options = (query && query.options) ? query.options : null
        let count = yield Owner.countDocuments(where, select, options)
        if (count) {
          return Promise.resolve(count)
        }
        return Promise.resolve(0)
      } catch (err) {
        return Promise.reject(new Error(`internal error: ${err.message}`))
      }
    })
    return Promise.resolve(tasks()).asCallback(callback)
  }
}

module.exports = Db
