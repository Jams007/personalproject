'use strict'
const utils = {
  connectToDatabase
}

/**
 * @description connect to the databases
 * @method connectToDatabase
 * @param {} none.
 * @return {boolean} true.
 */
async function connectToDatabase (db) {
  await db.connect()
  return Promise.resolve()
}

module.exports = utils
