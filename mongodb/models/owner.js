'use strict'
/*
  This model represents a Event
*/
const mongoose = require('bluebird').promisifyAll(require('mongoose'))
const timestamps = require('mongoose-timestamp')

const Schema = mongoose.Schema

let OwnerSchema = new Schema({
  name: String,
  shortName: String,
  events: [{
    type: Schema.Types.ObjectId,
    ref: 'Event'
  }]
})

OwnerSchema.plugin(timestamps)

module.exports = mongoose.model('Owner', OwnerSchema)
