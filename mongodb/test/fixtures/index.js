'use strict'
const casual = require('casual')
const fixtures = {

  /**
   * @description get a test uuid.
   * @param {number} n. the number of test user we wanna get.
   */
  getUUID () {
    return uuid.v4()
  },

	/**
   * @description get a test event.
   */
  getEvent () {
    return {
      date: new Date(),
      imgURL: `https://${uuid.v4()}.com/${uuid.v4()}.png`,
      eventURL
      categoryId: this.getUUID(),
      popularity: 5,
      address: casual.address,
      name: casual.title,
      description: casual.description,
      shortDescription: casual.short_description,
      eventURL: `https://${uuid.v4()}.com/${uuid.v4()}.com`,
      location: {
       type: "Point",
       coordinates: [36.098948, -112.110492]
      }
    }
  },

  /**
   * @description get multiple test events.
   * @param {number} n. the number of test user we wanna get.
   */
  getEvents (n) {
    let events = []
    while (n-- > 0) {
      let event = this.getEvent()
      events.push(event)
    }
    return events
  }
}