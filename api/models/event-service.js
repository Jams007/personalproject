const services = {
  listEvents,
  getEvent
}

/**
 * @description get all notifications of a user.
 * @function listEvents
 * @param {object} token, authentication token with information of the user.
 * @param {object} user, a postgresql user object
 * @param {object} db, a mongo db library object.
 * @param {number} page, wich page of all the notifications the user wants.
 * @param {number} limit, how many notifications does the user wants.
 * @return {array} notifications, all the notifications of the user.
 */
async function listEvents (params, db) {
  let offset = 1
  if (params && (params.offset) && (parseInt(params.offset) >= 0)) {
    offset = params.offset
  }
  offset = parseInt(offset)
  let limit = 7
  if (params && (params.limit) && (parseInt(params.limit) >= 0)) {
    limit = params.limit
  }
  limit = parseInt(limit)
  let skip = (offset - 1) * limit
  let query = {
    where: {},
    options: {
      skip,
      limit,
      sort: { date: 1 }
    }
  }
  query.where = getQueryParams(params)
  let eventsPromise = db.listEvents(query)
  let objectsCountPromise = db.countEvents(query)
  let promises = await Promise.all([eventsPromise, objectsCountPromise])
  let [events, objectsCount] = promises
  let totalPages = Math.ceil(objectsCount / limit) // Math.ceil() devuelve el entero más pequeño mayor o igual a un número
  let pagination = {
    totalPages,
    objectsCount
  }
  if (offset > totalPages) {
    offset = totalPages
  }
  if ((offset - 1) > 0) {
    pagination.previousPage = offset - 1
  }
  pagination.currentPage = offset
  if ((offset + 1) <= totalPages) {
    pagination.nextPage = offset + 1
  }
  return Promise.resolve({pagination, events})
}

async function getEvent (id, db) {
  let event = await db.getEvent(id)
  return Promise.resolve(event)
}

function getQueryParams (params) {
  let whereQuery = {}
  if (params.categoryId) {
    whereQuery.categoryId = params.categoryId
  }
  if (params && params.lat && params.lon) {
    whereQuery.location = {
      $near: {
        $maxDistance: 1000,
        $geometry: {
          type: 'Point',
          coordinates: [params.lon, params.lat]
        }
      }
    }
  }
  if (params.isVIP) {
    whereQuery.isVIP = params.isVIP
  }
  if (params.isActive) {
    whereQuery.isActive = params.isActive
  }
  if (params.owners) {
    whereQuery._id = { $in: params.owners }
  }
  if (params.events) {
    whereQuery._id = { $in: params.events }
  }
  return whereQuery
}

module.exports = services
