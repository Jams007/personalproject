const { ApolloServer } = require('apollo-server-micro')
const { router, get, post } = require('microrouter')
const microCors = require('micro-cors')
const { typeDefs } = require('./typedefs')
const { resolvers } = require('./resolvers')
const {connectToDataBase, generateCategoriesModel, generateEventsModel, generateOwnersModel} = require('./models')
const cors = microCors({
  allowMethods: ['PUT', 'POST', 'GET', 'DELETE'],
  allowHeaders: [['company', 'X-Requested-With', 'Access-Control-Allow-Origin', 'X-HTTP-Method-Override', 'Content-Type', 'Authorization', 'Accept', 'x-access-token']]
})
const env = process.env.NODE_ENV || 'development'
const graphqlPath = '/entrete'
const apolloServer = new ApolloServer({
  typeDefs, 
  resolvers, 
  graphiql: (env === 'dev' ? true : false),
  context: ({ req }) => {
    // get the user token from the headers
    const token = req.headers.authentication || 'not available';
    
    // try to retrieve a user with the token
    // *const user = getUser(token);
  
    // optionally block the user
    // we could also check user roles/permissions here
    // *if (!user) throw new AuthenticationError('you must be logged in to query this schema');	
  
    // add the user to the context
    return {
      token, 
      models: {
        Categories: generateCategoriesModel({ token }),
        Events: generateEventsModel({ token }),
        Owners: generateOwnersModel({ token })
      }
    }
  }
})
const graphqlHandler = cors(apolloServer.createHandler({ path: graphqlPath }))


connectToDataBase()

module.exports = router(
  get('/', (req, res) => 'Welcome!'),
  post(graphqlPath, graphqlHandler),
  get(graphqlPath, graphqlHandler)
)
