const services = {
  listCategories,
  getCategory
}

/**
 * @description get all notifications of a user.
 * @function listCategories
 * @param {object} token, authentication token with information of the user.
 * @param {object} user, a postgresql user object
 * @param {object} db, a mongo db library object.
 * @param {number} page, wich page of all the notifications the user wants.
 * @param {number} limit, how many notifications does the user wants.
 * @return {array} notifications, all the notifications of the user.
 */
async function listCategories (params, db) {
  let categories = await db.listCategories()
  return Promise.resolve({categories})
}

async function getCategory (id, db) {
  let category = await db.getCategory(id)
  return Promise.resolve(category)
}

module.exports = services
