'use strict'
/*
	cargamos las variables de entorno a traves de dotenv, con su funcion config
*/
require('dotenv').config()
/*
  instanciamos el objeto de micro, e importamos el archivo principal del micro servicio de authentication
  este archivo contiene unicamente las rutas de este servicio junto con la logica y funciones externas necesarias
*/
const micro = require('micro')
const event = require('./api/graphqlServer.js')
/*
  creamos un objeto de micro pasando por parametro el archivo principal del micro servicio de authentication
  y arrancamos el servicio asignandole un puerto para que escuche.
  Este puerto vienen dado desde el archivo ecosystem.config.js, en el cual al mandar a ejecutar este archivo, se
  le asigna un puerto especifico unicamente para este microservicio
*/
const eventServer = micro(event)
eventServer.listen(process.env.PORT)
