'use strict'
const {each} = require('lodash')

const eventUtils = {
  getEvents,
  getCategories
}

/**
 * @description get all notifications of a user.
 * @function getEvents
 * @param {object} token, authentication token with information of the user.
 * @param {object} user, a postgresql user object
 * @param {object} db, a mongo db library object.
 * @param {number} page, wich page of all the notifications the user wants.
 * @param {number} limit, how many notifications does the user wants.
 * @return {array} notifications, all the notifications of the user.
 */
async function getEvents (params, db) {
  let page = 1
  if (params && (params.page) && (parseInt(params.page) >= 0)) {
    page = params.page
  }
  page = parseInt(page) || 1
  let limit = 7
  let skip = (page - 1) * limit
  let query = {
    where: {},
    options: {
      skip,
      limit,
      sort: { createdAt: -1 }
    }
  }
  if (params.categoryId) {
    query.where.categoryId = params.categoryId
  }
  if (params && params.lat && params.lon) {
    query.where.location = {
      $near: {
        $maxDistance: 1000,
        $geometry: {
          type: 'Point',
          coordinates: [params.lon, params.lat]
        }
      }
    }
  }
  let events = await db.listEvents(query)
  let objectsInDB = await db.countEvents(query)
  let totalPages = Math.ceil(objectsInDB / limit) // Math.ceil() devuelve el entero más pequeño mayor o igual a un número
  let pagination = {
    totalPages,
    objectsInDB
  }
  if (page > totalPages) {
    page = totalPages
  }
  if ((page - 1) > 0) {
    pagination.previousPage = page - 1
  }
  pagination.currentPage = page
  if ((page + 1) <= totalPages) {
    pagination.nextPage = page + 1
  }
  return Promise.resolve({totalPages, objectsInDB, pagination, events})
}

/**
 * @description get all notifications of a user.
 * @function getCategories
 * @param {object} token, authentication token with information of the user.
 * @param {object} user, a postgresql user object
 * @param {object} db, a mongo db library object.
 * @param {number} page, wich page of all the notifications the user wants.
 * @param {number} limit, how many notifications does the user wants.
 * @return {array} notifications, all the notifications of the user.
 */
async function getCategories (params, db) {
  console.log('holaa')
  let categories = await db.listCategories()
  let promises = []
  each(categories, (category) => {
    let promise = getEvents({categoryId: category._id}, db)
    promises.push(promise)
  })
  let response = await Promise.all(promises)
  each(categories, (category, index) => {
    category.events = response[index].events
  })
  return Promise.resolve({categories})
}

module.exports = eventUtils
