'use strict'
const MongoDB = require('../../mongodb')
const utils = require('../utils/utils')
const DataLoader = require('dataloader')
const eventService = require('./event-service')
const ownersService = require('./owners-service')
const categoryService = require('./category-service')
const {groupBy} = require('lodash')
let db

async function connectToDataBase () {
  db = new MongoDB()
  utils.connectToDatabase(db)
}

const categoryEventsDataLoader = new DataLoader(async (categoryIds) => {
  let query = {
    _id: {$in: categoryIds}
  }
  let events = await db.listEvents(query)
  const eventsById = groupBy(events, 'categoryId')
  return categoryIds.map(categoryId => eventsById[categoryId])
})

const ownerEventsDataLoader = new DataLoader(async (eventIds) => {
  let query = {
    _id: { $in: eventIds }
  }
  let events = await db.listEvents(query)
  const eventsById = groupBy(events, '_id')
  return eventIds.map(ownerId => eventsById[ownerId])
})

const ownersDataLoader = new DataLoader(async (ownerIds) => {
  let query = {
    _id: { $in: ownerIds }
  }
  let owners = await db.listOwners(query)
  const ownersById = groupBy(owners, '_id')
  return ownerIds.map(ownerId => ownersById[ownerId])
})

const generateCategoriesModel = ({ token }) => ({
	listCategories: async (params) => {
    let response = await categoryService.listCategories(params, db)
    return response
  },
	getCategory: async (id) => {
    let response = await categoryService.getCategory(id, db)
    return response
  }
})

const generateEventsModel = ({ token }) => ({
	listEvents: async (params) => {
    let response = await eventService.listEvents(params, db)
    return response
  },
	getEvent: async (id) => {
    let response = await eventService.getEvent(id, db)
    return response
  }
})

const generateOwnersModel = ({ token }) => ({
	listOwners: async (params) => {
    let response = await ownersService.listOwners(params, db)
    return response
  },
	getOWner: async (id) => {
    let response = await ownersService.getOwner(id, db)
    return response  
  }
})

module.exports.connectToDataBase = connectToDataBase
module.exports.categoryEventsDataLoader = categoryEventsDataLoader
module.exports.ownersDataLoader = ownersDataLoader
module.exports.ownerEventsDataLoader = ownerEventsDataLoader
module.exports.generateCategoriesModel = generateCategoriesModel
module.exports.generateEventsModel = generateEventsModel
module.exports.generateOwnersModel = generateOwnersModel
