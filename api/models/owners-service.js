const services = {
  getOwner,
  listOwners
}

async function getOwner (id, db) {
  let owner = await db.getOwner(id)
  return Promise.resolve(owner)
}

async function listOwners (params, db) {
  let offset = 1
  if (params && (params.offset) && (parseInt(params.offset) >= 0)) {
    offset = params.offset
  }
  offset = parseInt(offset)
  let limit = 7
  if (params && (params.limit) && (parseInt(params.limit) >= 0)) {
    limit = params.limit
  }
  limit = parseInt(limit)
  let skip = (offset - 1) * limit
  let query = {
    where: {},
    options: {
      skip,
      limit,
      sort: { createdAt: -1 }
    }
  }
  query.where = getQueryParams(params)
  let ownersPromise = db.listOwners(query)
  let objectsCountPromise = db.countOwners(query)
  let promises = await Promise.all([ownersPromise, objectsCountPromise])
  let [owners, objectsCount] = promises
  let totalPages = Math.ceil(objectsCount / limit) // Math.ceil() devuelve el entero más pequeño mayor o igual a un número
  let pagination = {
    totalPages,
    objectsCount
  }
  if (offset > totalPages) {
    offset = totalPages
  }
  if ((offset - 1) > 0) {
    pagination.previousPage = offset - 1
  }
  pagination.currentPage = offset
  if ((offset + 1) <= totalPages) {
    pagination.nextPage = offset + 1
  }
  return Promise.resolve({pagination, owners})
}

function getQueryParams (params) {
  let whereQuery = {}
  if (params.events) {
    whereQuery._id = { $in: params.events }
  }
  return whereQuery
}

module.exports = services
