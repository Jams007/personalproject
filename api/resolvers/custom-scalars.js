const { GraphQLScalarType } = require('graphql')
const { Kind } = require('graphql/language')
// const moment = require('moment')

module.exports = {
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    parseValue (value) {
      return new Date(value) // value from the client
    },
    serialize (value) {
      return value // value sent to the client
      // return moment(value).format('DD-MM-YYYY hh:mm')
    },
    parseLiteral (ast) {
      if (ast.kind === Kind.INT) {
        return new Date(ast.value) // ast value is always in string format
      }
      return null
    }
  })
}
