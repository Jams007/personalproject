'use strict'
const {send, json} = require('micro')
const microCors = require('micro-cors')
const cors = microCors({
  allowMethods: ['PUT', 'POST', 'GET', 'DELETE'],
  allowHeaders: [['company', 'X-Requested-With', 'Access-Control-Allow-Origin', 'X-HTTP-Method-Override', 'Content-Type', 'Authorization', 'Accept', 'x-access-token']]
})
const HttpHash = require('http-hash')
const env = process.env.NODE_ENV || 'production'
const utils = require('./lib/utils/utils')
const eventUtils = require('./lib/utils/event.utils')
const qs = require('querystring')
const URL = require('url')
const MongoDB = require('../mongodb')
const hash = HttpHash()
let dbMDB = new MongoDB()

utils.connectToDatabase(dbMDB)

/**
 * @description get a list of all publications.
 * @method getEvents
 * @param {Object} req. a request object.
 * @param {Object} res. a response object.
 * @param {Object} params. all the params of the request.
 * @return {promise} response, an object with the information of the user including refresh and access tokens.
 */
hash.set('GET /events', async function getEvents (req, res, params) {
  try {
    return send(res, 200, await eventUtils.getEvents(req.query, dbMDB))
  } catch (err) {
    console.log(err)
    return send(res, 500, { error: err.message })
  }
})

/**
 * @description get a list of all publications.
 * @method getCategories
 * @param {Object} req. a request object.
 * @param {Object} res. a response object.
 * @param {Object} params. all the params of the request.
 * @return {promise} response, an object with the information of the user including refresh and access tokens.
 */
hash.set('GET /events/categories', async function getCategories (req, res, params) {
  try {
    return send(res, 200, await eventUtils.getCategories(req.query, dbMDB))
  } catch (err) {
    console.log(err)
    return send(res, 500, { error: err.message })
  }
})

/**
 * @description Main router of the authentication-microservice using zeit/micro library
 * @description For query strings manipulation (route?value=1):
 * @description   # parse the url of the request object into a url object (URL.parse(url))
 * @description   # get the "query" attribute (qs.parse(url.query))
 * @description   # parse the "query" attribute into a query object
 * @description   # pass to the "hash.get" method the "url.pathname" (url without query strings)
 * @description For normal params manipulation (route/5):
 * @description   # declare the endpoint with "route/:param" structure
 * @description   # http-hash add all this types of params in the "params" attribute
 * @description For body params manipulation:
 * @description   # use the "json(req)" module of micro to get all the body params inside the request object
 * @description   # assign the json to a variable
 */
const handler = async (req, res) => {
  let { method, url } = req
  url = URL.parse(url)
  let query = qs.parse(url.query)
  let match = hash.get(`${method.toUpperCase()} ${url.pathname}`)
  if (match.handler) {
    try {
      req.query = query
      await match.handler(req, res, match.params)
    } catch (e) {
      const data = { error: e.message }
      send(res, 500, data)
    }
  } else {
    send(res, 404, { error: `endpoint not found ${process.env.PORT}` })
  }
}

module.exports = cors(handler)
