const {ownersDataLoader} = require('../models')

module.exports = {
  Query: {
    getEvent: async (root, {_id}, context) => {
      // let response = await mainService.getEvent(_id)
      let response = await context.models.Events.getEvent(_id)
      return response
    },
    listEvents: async (root, {offset, limit, isVIP, isActive, categoryId}, context) => {
      // let response = await mainService.listEvents({offset, limit, isVIP, isActive, categoryId})
      let response = await context.models.Events.listEvents({offset, limit, isVIP, isActive, categoryId})
      return response
    }
  },
  Event: {
    owners: async (parent) => {
      let response = await ownersDataLoader.load([parent.owners])
      return response
    }
  }
}
