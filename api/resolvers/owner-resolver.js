const {ownerEventsDataLoader} = require('../models')

module.exports = {
  Query: {
    getOwner: async (root, {_id}, context) => {
      // let response = await mainService.getOwner(_id)
      let response = await context.models.Owners.getOwner(_id)
      return response
    },
    listOwners: async (root, {page, limit}, context) => {
      // let response = await mainService.listOwners({page, limit})
      let response = await context.models.Owners.listOwners({page, limit})
      return response
    }
  },
  Owner: {
    events: async (parent) => {
      // let response = await mainService.listEvents({events: parent.events})
      // return response.events
      let response = await ownerEventsDataLoader.load([parent.events])
      return response
    }
  }
}
